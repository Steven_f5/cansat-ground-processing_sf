/*
 * RTP_ConfigDataSet.h
 */

#pragma once
#include <array>
#include <string>

/**  @ingroup RecordProcessing
 * @brief A class containing all RT Processing settings which could change from one run to another.
 */
class RTP_ConfigDataSet {
public:
	/** Create a data set initialized with 0 values */
	RTP_ConfigDataSet();
	/** Create a data set from configuration file
	 *  @param dataFile The path to the file to load.
	 */
	RTP_ConfigDataSet(const std::string& dataFile);

	double localReferentialOrigin_Latitude; /**< Latitude of the local referential origin (decimal degrees). */
	double localReferentialOrigin_Longitude; /**< Longitude of the local referential origin (decimal degrees). */
	double localReferentialOrigin_Altitude; /**< Altitude of the local referential origin (meters) as returned by the gps.*/
	double temperatureOffset;  /**< The offset to add to the temperature readings (°C) */
	double seaLevelPressure;   /**< The sea level pressure at operation site (hPa)*/
	double PressureOffest;   /**< The pressure to subtract from the on-board measurement to account for the accurate pressure measure at the launch site, and launch time. (hPa)*/
	double IMU_SampleFrequency;   /**< The IMU sample frequency (Hz).*/
	unsigned int CO2_NumSampleToAverage;   /**< The number of samples to use for moving average on the CO2_Concentration measurements.*/
	unsigned long transmissionPeriod;	/**<The minimum delay between two successive transmissions on the socket.*/

	friend std::ostream& operator<< (std::ostream& stream, const RTP_ConfigDataSet& set);

};

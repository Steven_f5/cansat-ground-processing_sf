/*
 * ImageGeolocation.h
 *
 *  Created on: 8 avr. 2019
 *      Author: Steven
 */

#pragma once
#include <IsaTwoGroundRecord.h>
#include "RecordProcessor/RecordProcessor.h"
#include "RTP_ConfigDataSet.h"
#include "CameraCalibrationDataSet.h"

/** @ingroup RecordProcessing
 *  @brief A processor which computes to geographical coordinates of the ground locations corresponding to the image corners (defining the viewed quadrilateral).
 */
class ImageGeolocation : public RecordProcessor<IsaTwoGroundRecord, IsaTwoGroundRecord> {
public:
	ImageGeolocation(const RTP_ConfigDataSet theRTP_Config, const CameraCalibrationDataSet theCameraCalib);
	virtual ~ImageGeolocation();
	/** Perform a calculation of geographical coordinates and store it using the base class'
	 *  storeResult() method.
	 * @param recordIn  The record to process.
	 * @param recordOut The record resulting from the processing.
	 * @return True if the process success.
	 */
	virtual bool doProcess(const IsaTwoGroundRecord& recordIn, IsaTwoGroundRecord& recordOut);
	/**
	 *
	 */
	void pixelGeolocation (array<double, 3> &pixel, const IsaTwoGroundRecord& recordIn);
private:
	RTP_ConfigDataSet RTP_Config;
	CameraCalibrationDataSet CameraCalib;
	array<array<double, 3>, 3> E;		/**<Euler angles matrix*/
	array<array<double, 3>, 3> RotationMatrix;		/**<Rotation matrix of a rotation from X toward Y around Z-axis, by angle between the IMU and the Camera*/
	double latDPM, longDPM;		/**<The number of latitude degrees/m in N-S direction*/ /**<The number of longitude degrees/m in E-W direction*/
	array<double, 3> pixelTL, pixelTR, pixelBL, pixelBR;
	double tempPixel;
};

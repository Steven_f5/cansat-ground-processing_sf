/*
 * DummyGroundRecordSource.cpp
 */

#include "DummyGroundRecordSource.h"
#include "RecordProcessor/RecordProcessor.h"
#include <math.h>
#include <exception>
#include "DebugUtils.h"
static constexpr bool DBG=false;

// Define variation ranges:
constexpr int Duration=15000;  			// Variation period in msec.
constexpr double MinTemperature=3.0;
constexpr double MaxTemperature=25.0;
constexpr double MinPressure=800;
constexpr double MaxPressure=1200;
constexpr double MinAltitude=0.0;
constexpr double MaxAltitude=1200.0;
constexpr double MinRoll=-M_PI/3;
constexpr double MaxRoll=M_PI/3;
constexpr double MinYaw=-M_PI;
constexpr double MaxYaw=M_PI;
constexpr double MinPitch=-M_PI/3;
constexpr double MaxPitch=M_PI/3;
constexpr double MinCO2_Concentration=300;
constexpr double MaxCO2_Concentration=1000;
constexpr double MinPos=-500;
constexpr double MaxPos=2000;

constexpr double OS_Latitude=50.805408; // CSPU.
constexpr double OS_Longitude=4.340244;
constexpr double DegreePerKm_NS=1.0/(111.111);
constexpr double DegreePerKm_EW=0.01424; // 1.0/(111.111*cos(OS_Latitude*PI/180.0));
constexpr double PositionVariationInKm=3;

constexpr double MinLong=OS_Longitude-PositionVariationInKm*DegreePerKm_EW;
constexpr double MaxLong=OS_Longitude+PositionVariationInKm*DegreePerKm_EW;
constexpr double MinLat=OS_Latitude-PositionVariationInKm*DegreePerKm_NS;
constexpr double MaxLat=OS_Latitude+PositionVariationInKm*DegreePerKm_NS;


DummyGroundRecordSource::DummyGroundRecordSource(
			RecordSource::visualFeedback_t feedback,
			unsigned long thePeriod)
   : RecordSource(feedback),
	 period(thePeriod),
	 anglePhase(AnglePhase_t::yaw)
{
	// let's go through the full range in Duration msec.
	deltaRoll=(MaxRoll-MinRoll)*period/Duration;
	deltaYaw=(MaxYaw-MinYaw)*period/Duration;
	deltaPitch=(MaxPitch-MinPitch)*period/Duration;
	deltaTempBMP=deltaTempCorrected=(MaxTemperature-MinTemperature)*period/Duration;
	deltaAltitude=(MaxAltitude-MinAltitude)*period/Duration;
	deltaPressure=(MaxPressure-MinPressure)*period/Duration;
	deltaPosX=deltaPosY=deltaPosZ=(MaxPos-MinPos)*period/Duration;
	deltaLat=(MaxLat-MinLat)*period/Duration;
	deltaLong=(MaxLong-MinLong)*period/	Duration;
	deltaLatCorners=deltaLat/2.0;
	deltaLongCorners=deltaLong/2.0;
	deltaCO2=(MaxCO2_Concentration-MinCO2_Concentration)*period/Duration;

	record.clear();
	record.timestamp=35000;
	record.groundAHRS_Roll=0;
	record.groundAHRS_Yaw=0;
	record.groundAHRS_Pitch=0;
	record.temperatureCorrected=12.0;
	record.temperatureBMP=9.5;
	record.pressure=1066.0;
	record.altitude=300;
	record.CO2_Concentration=300;
	record.GPS_LatitudeDegrees=OS_Latitude;
	record.GPS_LongitudeDegrees=OS_Longitude;
	record.GPS_Measures=true;
	record.positionLocal[0]=100;
	record.positionLocal[1]=-10;
	record.positionLocal[2]=-200;
	constexpr double OnekmInDegree=1/111.0;
	record.imgCornerLong[0]=OS_Longitude+OnekmInDegree/2.0;
	record.imgCornerLong[1]=OS_Longitude+OnekmInDegree/3.0;
	record.imgCornerLong[2]=OS_Longitude-OnekmInDegree/4.0;
	record.imgCornerLong[3]=OS_Longitude-2*OnekmInDegree/3.0;
	record.imgCornerLat[0]=OS_Latitude+OnekmInDegree/2.0;
	record.imgCornerLat[1]=OS_Latitude-OnekmInDegree/2.0;
	record.imgCornerLat[2]=OS_Latitude-OnekmInDegree/3.0;
	record.imgCornerLat[3]=OS_Latitude+OnekmInDegree/4.0;

	cout << "Long/Lat    : " << record.GPS_LongitudeDegrees <<"°/" << record.GPS_LatitudeDegrees  << "°, [" << MinRoll << "," << MaxRoll << "]" << endl;
	cout << "Pos         : (" << record.positionLocal[0] << ',' << record.positionLocal[1] <<"," << record.positionLocal[2] << "), [" << MinPos << "," << MaxPos << "]" << endl;
	cout << "Roll        : " << record.groundAHRS_Roll << ", [" << MinRoll << "," << MaxRoll << "]" << endl;
	cout << "Yaw         : " << record.groundAHRS_Yaw << ", [" << MinYaw << "," << MaxYaw << "]" << endl;
	cout << "Pitch       : " << record.groundAHRS_Pitch << ", [" << MinPitch << "," << MaxPitch << "]" << endl;
	cout << "Temp. BMP.  : " << record.temperatureBMP << ", [" << MinTemperature << "," << MaxTemperature << "]" << endl;
	cout << "Temp. corr. : " << record.temperatureCorrected << ", [" << MinTemperature << "," << MaxTemperature << "]" << endl;
	cout << "Pressure    : " << record.pressure << ", [" << MinPressure << "," << MaxPressure << "]" << endl;
	cout << "Altitude    : " << record.altitude << ", [" << MinAltitude << "," << MaxAltitude << "]" << endl;
}

DummyGroundRecordSource::~DummyGroundRecordSource() {
	// TODO Auto-generated destructor stub
}

bool DummyGroundRecordSource::changeValue(double &value, double &delta, double lowerBound, double upperBound) {
	LOG_IF(DBG,DEBUG) << "Value: " << value << ", delta=" << delta  << " [" << lowerBound << "," << upperBound <<"]";
	value += delta;
	if ((value > upperBound) || (value < lowerBound)) {
		delta = -delta;
		value += 2 * delta;
		return true;
	}
	else return false;
}

bool DummyGroundRecordSource::feedOneRecordToProcessor(
		Processor& processor,
		bool& recordProcessed,
		bool& success) {
	record.timestamp+=period;

	// data variation.
	switch(anglePhase) {
	case roll:
		if (changeValue(record.groundAHRS_Roll,  deltaRoll, MinRoll, MaxRoll))
			anglePhase=all;
		break;
	case yaw:
		if (changeValue(record.groundAHRS_Yaw,  deltaYaw,MinYaw, MaxYaw))
			anglePhase=pitch;
		break;
	case pitch:
		if (changeValue(record.groundAHRS_Pitch,  deltaPitch, MinPitch, MaxPitch))
			anglePhase=roll;
		break;
	case all:
		changeValue(record.groundAHRS_Roll,  deltaRoll, MinRoll, MaxRoll);
		changeValue(record.groundAHRS_Yaw,  deltaYaw, MinYaw, MaxYaw);
		changeValue(record.groundAHRS_Pitch,  deltaPitch, MinPitch, MaxPitch);
		break;
	default:
		throw runtime_error("Unexpected anglePhase in DummyGroundRecordSource");
	} // switch

	changeValue(record.temperatureBMP,deltaTempBMP, MinTemperature, MaxTemperature);
	changeValue(record.temperatureCorrected,deltaTempCorrected, MinTemperature, MaxTemperature);
	changeValue(record.pressure,deltaPressure, MinPressure, MaxPressure);
	changeValue(record.altitude, deltaAltitude, MinAltitude, MaxAltitude);
	changeValue(record.GPS_LongitudeDegrees, deltaLong, MinLong, MaxLong);
	changeValue(record.GPS_LatitudeDegrees, deltaLat, MinLat, MaxLat);
	changeValue(record.positionLocal[0], deltaPosX, MinPos, MaxPos);
	changeValue(record.positionLocal[1], deltaPosY, MinPos, MaxPos);
	changeValue(record.positionLocal[2], deltaPosZ, MinPos, MaxPos);
	changeValue(record.CO2_Concentration, deltaCO2, MinCO2_Concentration, MaxCO2_Concentration);
	changeValue(record.altitude_Corrected, deltaAltitude, MinAltitude, MaxAltitude-100);
	for (unsigned long i =0; i<4; i++) {
		changeValue(record.imgCornerLong[i], deltaLongCorners, MinLong, MaxLong);
		changeValue(record.imgCornerLat[i], deltaLatCorners, MinLat, MaxLat);
	}

	// Print record.
	stringstream s;
	record.printCSV(s);
	processor.processString(s.str());
	success=true;
	recordProcessed=true;
	return true;
}

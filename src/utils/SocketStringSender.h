/*
 * SocketStringSender.h
 */

#pragma once
// WARNING: Keep include of asio.hpp in first position to avoid
//          compilation issues
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wconversion"
#ifdef __MINGW32__
#  pragma GCC diagnostic ignored "-Wcast-function-type"
#endif
#include <asio.hpp>
#pragma GCC diagnostic pop

#include <string>
#include <thread>
#include "MinGW_ThreadProxy.h"
#include <queue>
#include <mutex>

/** @ingroup Utilities
 *  @brief A gateway class that manages a TCP connexion and sends data into a TCP socket.
 *
 *  This class opens a socket server, accepts a single external connection
 *  and sends everything to the socket as long as the client is connected.
 *   If no connection is open, or the client disconnects, it either:
 *   - blocks until another connection request is received, blocking any activity in this thread
 *     (if option=suspendOnClientDisconnection)
 *   - just ignores the incoming strings as long as a connexion is not established (otherwise).
 *
 */
class SocketStringSender  {
public:
	typedef enum { 	suspendOnClientDisconnection,
					proceedDuringClientDisconnection } ClientDisconnectOption_t;
	/** Constructor
	 *  @param thePortNbr The number of the port on which to accept TCP connections.
	 *  @param option	  If option is suspendOnClientDeconnexion, the processing is suspended
	 *  				  when the client is not connected and resumed afterward.
	 *  				  This is the appropriate mode when processing offline
	 *  				  data (it would not make sense not to wait for a client to ingest them).
	 *  				  If proceedDuringClientDisconnection, incoming records are just passed to the
	 *  				  next processor (if any) when no client is connected.
	 *  				  This is the appropriate mode when processing a real-time feed: it would
	 *  				  not make sense to miss data because the client disconnected.
	 *  @param visualFeadback If withVisualFeedback, the processor prints some feedback on cout:
	 *  						a '.' for each record transferred,
	 *  						a '!' for each record ignored because no client is connected and
	 *  						      suspendOnClientDeconnexion is false.
	 */
	SocketStringSender(	unsigned short thePortNbr,
				ClientDisconnectOption_t option=suspendOnClientDisconnection,
					bool visualFeadback=true);

	virtual ~SocketStringSender();
	/** This method should be called just before the first call to
	 *  of the send() method.
	 *  @return true if preparation was successful and the processor is ready
	 *          to accept records, false otherwise.
	 */
	virtual bool prepare();

	/** Just send the provided record through the socket, if any client is connected.
	 * @param str  The string to process. It is assumed NOT to be terminated by a '\\n'.
	 * @return Always true.
	 */
	bool send(const std::string& str) ;

	/** Call this method as soon as possible after the last record is processed, to release
	 *  the socket resources
	 *  This method will be called at the latest in the destructor, but a well behaved user could
	 *  have it called earlier. This method MUST support multiple calls.
	 */
    virtual void terminate();
protected:
	/** If the listening thread is running, signal it to terminate and wait for termination.
	 */
	void stopServerThread();

	/** Add a record to the output queue for transmission
	 *  @param record  The record to added
	 *  @param counter A counter, incremented by the caller, to allow for some output formatting.
	 */
	void SendRecordToOutputQueue(const std::string& record, unsigned int &counter);

	/** Ignore a record (as far as the output queue is concerned: the record still is transferred
	 *  to the next processor). This method is called on record received while client is disconnected
	 *  in case the processor runs with suspendOnClientDeconnexion=false.
	 *  @param str  	The string to be ignored
	 *  @param counter 	A counter, incremented by the caller, to allow for some output formatting.
	 */
	void IgnoreRecord(const std::string& str, unsigned int &counter);

	/** Make all required checks about the client connection. If the client is currently
	 *  disconnected:
	 *    - if the object was created with parameter doSuspendOnClientDisconnection, this
	 *      method does not wait for the client to connect.
	 *    - otherwise it waits until the client connects (or the user cancels the processing).
	 *  @return True if the client is connected, false otherwise.
	 */
	bool checkClientIsConnected();

    /** @name Methods that may only be called from the serverThread
     *  Those methods must all be called from the same thread (they don't make use of mutex,
     *  and rely on the single thread asynchronous architecture of ASIO
     *  @{
     */
	/** The main function of the server thread */
	void ServerThread_Function();
    /** The callback function called by the ASIO io_context on completion of an asynchronous
     *  write request.
     */
	void RecordWritten_Handler(const asio::error_code& error, size_t bytesTransferred);
	 /** The callback function called by the ASIO io_context on completion of an asynchronous
	  *  accept connection request.
	  */
	void ConnectionAccepted_Handler(const asio::error_code& error);
	/** A utility function called by the callback functions in case of error. It registers the
	 *  client deconnexion and possibly prints some status information on cout (depending on the
	 *  compilation symbol CONNECTION_STATUS_ON_COUT possibly defined in the implementation file).
	 */
	void DisconnectClient();

	/** This method waits for, and accepts, the client connexion.
	 *  Should the port already be in use, the next 10 ones are tried and possibly used.
	 */
	void AcceptClientConnexion();

	/** @} End of group of methods reserved to serverThread */
private:
	unsigned short portNbr;					/**< The port on which an incoming connections will be accepted */
	bool doSuspendOnClientDisconnection;	/**< If true, processing is blocked when client is disconnected */
	std::thread *serverThread;				/**< Pointer to the thread object in charge of accepting
												 connections and running the asynchronous write
												 requests */
	asio::io_context io_context;			/**< Asio context for all I/O operations */
	asio::ip::tcp::socket *tcpSocket;		/**< Pointer to the socket to the client. Valid only if
												 clientConnected = true. This pointer and object may
												 only be modified by the serverThread. */
	std::atomic<bool> listenForConnexion;   /**< Flag used by the listening thread: if false, termination is required.*/
	std::atomic<bool> clientConnected;   	/**< True if a client is currently connected */
	std::queue<std::string> outputQueue;	/**< The queue of strings to be sent to the socket */
	std::mutex outputQueueMutex;	        /**< The mutex for avoiding thread conflicts on the output queue */
	bool visualFeedback;					/**< Inform the user on cout about connection/disconnection status */
};
